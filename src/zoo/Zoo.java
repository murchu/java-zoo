package zoo;

import animals.Animal;
import zookeepers.Zookeeper;

public class Zoo {
	
	Animal[] animalPopulation;
	Zookeeper[] zookeeperStaff;
	
	// find all animals
	Animal[] findAnimals() {
		return null;
	}
		
	// find all zookeepers
	Zookeeper[] findZookeepers() {
		return null;
	}
	
	void addAnimal(Animal newAnimal) {
		// add animal to list
	}
	
	void addZookeeper(Zookeeper newZookeeper) {
		// 
	}
	
	void updateAnimal(Animal newAnimalDetails) {
		// 
	}
	
	void updateZookeeper(Zookeeper newZookeeperDetails) {
		//
	}
	
	void assignAnimalToZookeeper(Animal animal, Zookeeper zookeeper) {
		// assign animal to zookeeper
	}
	
}

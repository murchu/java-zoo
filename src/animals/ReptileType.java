package animals;

public interface ReptileType {
	
	void bite();
	
	void flickWithTail();

}

package animals;

public interface InsectType {
	
	void fly();
		
	void makeNoise();

}

package animals;

import java.util.ArrayList;
import java.util.Date;

import zookeepers.Medication;
import zookeepers.Zookeeper;

public class Animal {
	
	Date dateOfBirth;
	Date dateOfArrival;
	String gender;
	ArrayList<Animal> parents;
	ArrayList<Animal> offspring;
	ArrayList<Medication> medication;
	Boolean vaccinated;
	int exhibitNumber;
	Zookeeper zookeeper;
	
}

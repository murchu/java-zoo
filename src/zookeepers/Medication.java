package zookeepers;

public class Medication {
	
	String dosageQuantity;
	String dosesPerDay;
	
	public Medication(String dosageQuantity, String dosesPerDay) {
		super();
		this.dosageQuantity = dosageQuantity;
		this.dosesPerDay = dosesPerDay;
	}

	public String getDosageQuantity() {
		return dosageQuantity;
	}
	
	public void setDosageQuantity(String dosageQuantity) {
		this.dosageQuantity = dosageQuantity;
	}
	
	public String getDosesPerDay() {
		return dosesPerDay;
	}
	
	public void setDosesPerDay(String dosesPerDay) {
		this.dosesPerDay = dosesPerDay;
	}
	
}

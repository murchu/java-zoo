package ui;

import utils.Utils;
import zoo.Zoo;

import java.util.Scanner;  


public class ZooSystem {
	
	static void showOptions() {
		System.out.println("\n  Welcome to the Zoo system! Select an option:\n\n"
				+ "    1: Search for animal(s)\n"
				+ "    2: Search for zookeeper(s)\n"
				+ "    3: Add new animal(s)\n"
				+ "    4: Add new zookeeper(s)\n"
				+ "    5: Update animal(s)\n"
				+ "    6: Update zookeeper(s)\n"				
				+ "    0: Exit system\n\n"
				+ "  Your selection: "
				);
	}	
	
	public static void main(String[] args) {
		// this is main UI for zoo system
		
		Zoo zoo = new Zoo();
		Utils.initialiseTestData(zoo);
		Scanner sc = new Scanner(System.in);
		// start UI
		
		boolean exit = false;
		while(!exit) {
			showOptions();
			int option = sc.nextInt();
			switch(option) {
			case 1:
				System.out.println("Search for animal(s)");
				// implement action for option
				break;
			case 2:
				System.out.println("Search for zookeeper(s)");
				// implement action for option
				break;
			case 3:
				System.out.println("Add new animal(s)");
				// implement action for option
				break;
			case 4:
				System.out.println("Add new zookeeper(s)");
				// implement action for option
				break;
			case 5:
				System.out.println("Update animal(s)");
				// implement action for option
				break;
			case 6:
				System.out.println("Update zookeeper(s)");
				// implement action for option
				break;
			case 0:
				System.out.println("\nYou've chosen to exit system. Connection to system closed.");
				exit = true;
				break;
			default:
				System.out.println("You've chosen an incorrect option, please choose again");
				break;
			}
		}
	}

}
